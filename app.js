var express = require("express"),
        app = express(),
        bodyParser = require("body-parser"),
        mongoose = require("mongoose");
mongoose.connect("mongodb://localhost/yelp_camp");

app.use(bodyParser.urlencoded({extended: true}));
app.set("view engine", "ejs");

// Schema Setup
var campgroundSchema = new mongoose.Schema({
    name: String,
    image: String,
    description: String
});

var Campground = mongoose.model("Campground", campgroundSchema);

//Campground.create({
    //name: "White River",
    //image: "https://farm4.staticflickr.com/3002/2386125285_2c05889d2a.jpg",
    //description: "Awesome campground at mt rainier"
//}, function(err, campground){
    //if(err){
        //console.log(err);
    //} else{
        //console.log("Newly created campground: ");
        //console.log(campground);
    //}
//});

//var campgrounds = [
    //{name: "Salmon Creek", image: "https://pixabay.com/get/e837b1072af4003ed1584d05fb1d4e97e07ee3d21cac104496f5c97ba6edb7bd_340.jpg"},
    //{name: "White River", image: "https://farm4.staticflickr.com/3002/2386125285_2c05889d2a.jpg"},
    //{name: "Ohanapecosh", image: "https://farm4.staticflickr.com/3197/3062217024_7958f241e5.jpg"},
    //{name: "Salmon Creek", image: "https://pixabay.com/get/e837b1072af4003ed1584d05fb1d4e97e07ee3d21cac104496f5c97ba6edb7bd_340.jpg"},
    //{name: "White River", image: "https://farm4.staticflickr.com/3002/2386125285_2c05889d2a.jpg"},
    //{name: "Ohanapecosh", image: "https://farm4.staticflickr.com/3197/3062217024_7958f241e5.jpg"}
//];

app.get("/", function(req, res){
    res.render("landing");
});


// Index - show all campgrounds
app.get("/campgrounds", function(req, res){
    //res.render("campgrounds", {campgrounds:campgrounds});
    // Get campgrounds from db then render
    Campground.find({}, function(err, allCampgrounds){
        if(err){
            console.log(err);
        } else{
            //console.log(campgrounds);
            res.render("index", {campgrounds:allCampgrounds});
        }
    });
});

// Create -add new campgrounds
app.post("/campgrounds", function(req, res){
    // Get data from form and add to campground array
    // Redirect back to campgrounds page
    var name = req.body.name;
    var image = req.body.image;
    var desc = req.body.description;
    var newCampground = {name: name, image: image, description: desc}
    // Create new campground and save to db
    Campground.create(newCampground, function(err, newlyCreated){
        if(err){
            console.log(err);
        } else {
            res.redirect("/campgrounds");
        }
    });
    // campgrounds.push(newCampground);

});

// New - show form to make campground
app.get("/campgrounds/new", function(req, res){
    res.render("new.ejs");
});

// Show - 
app.get("/campgrounds/:id", function(req, res){
    // res.send("Show pages comming soon");
    // Find campground with id
    Campground.findById(req.params.id, function(err, foundCampground){
        if (err){
            console.log(err);
        } else {
            res.render("show", {campground: foundCampground});
        }
    });
    // Render show page based off id
})


app.listen(3000, process.env.IP, function(){
    console.log("YelpCamp Server Started");
});

process.on('SIGINT', function(){ 
    process.exit(1);
});                                    
